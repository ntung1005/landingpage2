import React from "react";
import Grid from "@material-ui/core/Grid";
import Hidden from "@material-ui/core/Hidden";

const Strengths = () => {
    return (
        <div className={"strengths-area body-general"}>
            <Grid container direction="row" spacing={5} alignItems={"center"} style={{marginBottom:40}}>
                <Hidden smDown>
                    <Grid item md={6} sm={12}>
                        <div className={"strengths-area__image"}>
                            <img src={"https://static.nhadatmoi.net/default/for-broker.jpg"}/>
                        </div>
                    </Grid>
                </Hidden>
                <Grid item md={6} sm={12}>
                    <div className={"strengths-area__content"}>
                        <p className={"strengths-area__content__title title-general"}>Sứ mệnh với người môi giới </p>
                        <p className={"strengths-area__content__text description-general"}>Tin rao của bạn sẽ được tiếp cận một lượng khách
                            hàng lớn nhất bằng công nghệ. Đăng tin theo
                            cách
                            của bạn, còn lại để Nhà Đất Mới lo. Mang tới cho người bán và người mua những giao dịch kiểu
                            mới: nhanh, chuẩn, chính xác.</p>
                        <p className={"strengths-area__content__text description-general"}>Tin rao của bạn sẽ được tiếp cận một lượng khách
                            hàng lớn nhất bằng công nghệ. Đăng tin theo
                            cách
                            của bạn, còn lại để Nhà Đất Mới lo. Mang tới cho người bán và người mua những giao dịch kiểu
                            mới: nhanh, chuẩn, chính xác.</p>
                        <p className={"strengths-area__content__text description-general"}>Tin rao của bạn sẽ được tiếp cận một lượng khách
                            hàng lớn nhất bằng công nghệ. Đăng tin theo
                            cách
                            của bạn, còn lại để Nhà Đất Mới lo. Mang tới cho người bán và người mua những giao dịch kiểu
                            mới: nhanh, chuẩn, chính xác.</p>
                    </div>
                </Grid>

                <Hidden mdUp>
                    <Grid item md={6} sm={12}>
                        <div className={"strengths-area__image"}>
                            <img src={"https://static.nhadatmoi.net/default/for-broker.jpg"}/>
                        </div>
                    </Grid>
                </Hidden>
            </Grid   >

            <Grid container direction="row" spacing={5} alignItems={"center"} style={{marginBottom:40}}>

                <Grid item md={6}>
                    <div className={"strengths-area__content"}>
                        <p className={"strengths-area__content__title title-general"}>Sứ mệnh với người môi giới </p>
                        <p className={"strengths-area__content__text description-general"}>Tin rao của bạn sẽ được tiếp cận một lượng khách
                            hàng lớn nhất bằng công nghệ. Đăng tin theo
                            cách
                            của bạn, còn lại để Nhà Đất Mới lo. Mang tới cho người bán và người mua những giao dịch kiểu
                            mới: nhanh, chuẩn, chính xác.</p>
                        <p className={"strengths-area__content__text description-general"}>Tin rao của bạn sẽ được tiếp cận một lượng khách
                            hàng lớn nhất bằng công nghệ. Đăng tin theo
                            cách
                            của bạn, còn lại để Nhà Đất Mới lo. Mang tới cho người bán và người mua những giao dịch kiểu
                            mới: nhanh, chuẩn, chính xác.</p>
                        <p className={"strengths-area__content__text description-general"}>Tin rao của bạn sẽ được tiếp cận một lượng khách
                            hàng lớn nhất bằng công nghệ. Đăng tin theo
                            cách
                            của bạn, còn lại để Nhà Đất Mới lo. Mang tới cho người bán và người mua những giao dịch kiểu
                            mới: nhanh, chuẩn, chính xác.</p>
                    </div>
                </Grid>

                <Grid item md={6}>
                    <div className={"strengths-area__image"}>
                        <img src={"https://static.nhadatmoi.net/default/we-strong.jpg"}/>
                    </div>
                </Grid>
            </Grid>

            <Grid container direction="row" spacing={5} alignItems={"center"}>
                <Hidden smDown>
                    <Grid item md={6}>
                        <div className={"strengths-area__image"}>
                            <img src={"https://static.nhadatmoi.net/default/home-sell.jpg"}/>
                        </div>
                    </Grid>
                </Hidden>
                <Grid item md={6}>
                    <div className={"strengths-area__content"}>
                        <p className={"strengths-area__content__title title-general"}>Sứ mệnh với người môi giới </p>
                        <p className={"strengths-area__content__text description-general"}>Tin rao của bạn sẽ được tiếp cận một lượng khách
                            hàng lớn nhất bằng công nghệ. Đăng tin theo
                            cách
                            của bạn, còn lại để Nhà Đất Mới lo. Mang tới cho người bán và người mua những giao dịch kiểu
                            mới: nhanh, chuẩn, chính xác.</p>
                        <p className={"strengths-area__content__text description-general"}>Tin rao của bạn sẽ được tiếp cận một lượng khách
                            hàng lớn nhất bằng công nghệ. Đăng tin theo
                            cách
                            của bạn, còn lại để Nhà Đất Mới lo. Mang tới cho người bán và người mua những giao dịch kiểu
                            mới: nhanh, chuẩn, chính xác.</p>
                        <p className={"strengths-area__content__text description-general"}>Tin rao của bạn sẽ được tiếp cận một lượng khách
                            hàng lớn nhất bằng công nghệ. Đăng tin theo
                            cách
                            của bạn, còn lại để Nhà Đất Mới lo. Mang tới cho người bán và người mua những giao dịch kiểu
                            mới: nhanh, chuẩn, chính xác.</p>
                    </div>
                </Grid>

                <Hidden mdUp>
                    <Grid item md={6}>
                        <div className={"strengths-area__image"}>
                            <img src={"https://static.nhadatmoi.net/default/home-sell.jpg"}/>
                        </div>
                    </Grid>
                </Hidden>

            </Grid>
        </div>
    )
};

export default Strengths;
