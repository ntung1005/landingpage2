import React from "react";
import Grid from "@material-ui/core/Grid";

const Mission = () => {
    return (
        <div className={"mission-area body-general"}>
            <Grid container direction="row" spacing={3} alignItems={"center"}>
                <Grid item md={7}>
                    <div className={"mission-area__left"}>
                        <div className={"mission-area__left__content"}>
                            <p className={"mission-area__left__content__title title-general"}>Tầm nhìn</p>
                            <p className={"mission-area__left__content__text description-general"}>Nhà Đất Mới mong muốn trở thành một trong
                                những sàn thương mại điện tử hàng đầu Đông Nam
                                Á</p>

                            <div className={"mission-area__left__content__underline"}></div>
                        </div>

                        <div className={"mission-area__left__content"}>
                            <p className={"mission-area__left__content__title title-general"}>Sứ mệnh</p>
                            <p className={"mission-area__left__content__text description-general"}>
                                Sứ mệnh của chúng tôi là mang lại giải pháp thành công tốt nhất cho từng khách hàng cũng
                                như
                                là thành viên trong công ty. Với quy mô nhân sự, tài chính và công nghệ ngày một tốt
                                lên,
                                chúng tôi cũng mong muốn góp một cánh tay giúp đỡ các cá nhân, tổ chức gặp khó khăn,
                                từ đó
                                góp phần xây dựng một nước Việt Nam văn minh, giàu mạnh.
                            </p>

                            <div className={"mission-area__left__content__underline"}></div>
                        </div>
                    </div>
                </Grid>

                <Grid item md={5}>
                    <img src={"https://static.nhadatmoi.net/default/background-vison.jpg"}/>
                </Grid>
            </Grid>
        </div>
    )
};

export default Mission;
