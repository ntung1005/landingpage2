import React from "react";
import Grid from "@material-ui/core/Grid";

const AboutUs = () => {
    return (
        <div className={"aboutus-area body-general"}>
            <div className={"aboutus-area__title"}>
                <p className={"title-general"}>Đội ngũ nhà đất mới</p>
            </div>

            <Grid container direction="row" spacing={3}>

                <Grid item lg={3} md={4} sm={6} xs={12}>
                    <div className={"aboutus-area__card"}>
                        <div className={"aboutus-area__card__image"}>
                            <img src={"https://www.w3schools.com/howto/img_avatar.png"}/>
                        </div>

                        <p className={"aboutus-area__card__name"}>
                            Tony Vũ
                        </p>

                        <p className={"aboutus-area__card__position"}>
                            Project Manager
                        </p>
                    </div>
                </Grid>

                <Grid item lg={3} md={4} sm={6} xs={12}>
                    <div className={"aboutus-area__card"}>
                        <div className={"aboutus-area__card__image"}>
                            <img src={"https://www.w3schools.com/howto/img_avatar.png"}/>
                        </div>

                        <p className={"aboutus-area__card__name"}>
                            Tony Vũ
                        </p>

                        <p className={"aboutus-area__card__position"}>
                            Project Manager
                        </p>
                    </div>
                </Grid>

                <Grid item lg={3} md={4} sm={6} xs={12}>
                    <div className={"aboutus-area__card"}>
                        <div className={"aboutus-area__card__image"}>
                            <img src={"https://www.w3schools.com/howto/img_avatar.png"}/>
                        </div>

                        <p className={"aboutus-area__card__name"}>
                            Tony Vũ
                        </p>

                        <p className={"aboutus-area__card__position"}>
                            Project Manager
                        </p>
                    </div>
                </Grid>

                <Grid item lg={3} md={4} sm={6} xs={12}>
                    <div className={"aboutus-area__card"}>
                        <div className={"aboutus-area__card__image"}>
                            <img src={"https://www.w3schools.com/howto/img_avatar.png"}/>
                        </div>

                        <p className={"aboutus-area__card__name"}>
                            Tony Vũ
                        </p>

                        <p className={"aboutus-area__card__position"}>
                            Project Manager
                        </p>
                    </div>
                </Grid>

                <Grid item lg={3} md={4} sm={6} xs={12}>
                    <div className={"aboutus-area__card"}>
                        <div className={"aboutus-area__card__image"}>
                            <img src={"https://www.w3schools.com/howto/img_avatar.png"}/>
                        </div>

                        <p className={"aboutus-area__card__name"}>
                            Tony Vũ
                        </p>

                        <p className={"aboutus-area__card__position"}>
                            Project Manager
                        </p>
                    </div>
                </Grid>

                <Grid item lg={3} md={4} sm={6} xs={12}>
                    <div className={"aboutus-area__card"}>
                        <div className={"aboutus-area__card__image"}>
                            <img src={"https://www.w3schools.com/howto/img_avatar.png"}/>
                        </div>

                        <p className={"aboutus-area__card__name"}>
                            Tony Vũ
                        </p>

                        <p className={"aboutus-area__card__position"}>
                            Project Manager
                        </p>
                    </div>
                </Grid>

                <Grid item lg={3} md={4} sm={6} xs={12}>
                    <div className={"aboutus-area__card"}>
                        <div className={"aboutus-area__card__image"}>
                            <img src={"https://www.w3schools.com/howto/img_avatar.png"}/>
                        </div>

                        <p className={"aboutus-area__card__name"}>
                            Tony Vũ
                        </p>

                        <p className={"aboutus-area__card__position"}>
                            Project Manager
                        </p>
                    </div>
                </Grid>

                <Grid item lg={3} md={4} sm={6} xs={12}>
                    <div className={"aboutus-area__card"}>
                        <div className={"aboutus-area__card__image"}>
                            <img src={"https://www.w3schools.com/howto/img_avatar.png"}/>
                        </div>

                        <p className={"aboutus-area__card__name"}>
                            Tony Vũ
                        </p>

                        <p className={"aboutus-area__card__position"}>
                            Project Manager
                        </p>
                    </div>
                </Grid>




            </Grid>

        </div>
    )
};

export default AboutUs;
