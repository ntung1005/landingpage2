import React from "react";
import Grid from "@material-ui/core/Grid";

const Cards = () => {
    return (
        <div className={"cards-area body-general"}>
            <Grid container direction="row" spacing={3}>
                <Grid item lg={3} md={6} sm={6}>
                    <div className={"cards-area__card"}>
                        <div>
                            <img src={"https://static.nhadatmoi.net/default/success_2.svg"}/>
                        </div>

                        <p className={"cards-area__card__title"}>
                            Bán thành công hơn
                        </p>

                        <p className={"cards-area__card__description "}>
                            Dùng kỹ thuật SEO đưa dự án của bạn lên TOP với đội ngũ SEO SIÊU CHẤT
                        </p>
                    </div>
                </Grid>

                <Grid item lg={3} md={6} sm={6}>
                    <div className={"cards-area__card"}>
                        <div>
                            <img src={"https://static.nhadatmoi.net/default/sell_fast.svg"}/>
                        </div>

                        <p className={"cards-area__card__title"}>
                            Bán nhanh hơn
                        </p>

                        <p className={"cards-area__card__description"}>
                            Dự án của bạn tiếp cận nhiều người nhờ Công nghệ AI và Big Data
                        </p>
                    </div>
                </Grid>

                <Grid item lg={3} md={6} sm={6}>
                    <div className={"cards-area__card"}>
                        <div>
                            <img src={"https://static.nhadatmoi.net/default/sell_success.svg"}/>
                        </div>

                        <p className={"cards-area__card__title"}>
                            Bán thành hiệu quả
                        </p>

                        <p className={"cards-area__card__description"}>
                            Tích hợp hệ thống quảng cáo tự động vào tin rao của bạn giúp tiết kiệm chi phí
                        </p>
                    </div>
                </Grid>

                <Grid item lg={3} md={6} sm={6}>
                    <div className={"cards-area__card"}>
                        <div>
                            <img src={"https://static.nhadatmoi.net/default/marketing.svg"}/>
                        </div>

                        <p className={"cards-area__card__title"}>
                            Tiếp cận KH chuẩn
                        </p>

                        <p className={"cards-area__card__description"}>
                            Hệ thống chấm điểm tin rao giúp tin rao của bạn tiếp cận đúng đối tượng KH
                        </p>
                    </div>
                </Grid>
            </Grid>


        </div>
    )
};

export default Cards;
