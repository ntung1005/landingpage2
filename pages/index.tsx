import React from "react";
import Head from "next/head";

import {NextPage} from 'next';
import Banner from "../components/landingPage/Banner";
import Cards from "../components/landingPage/cards";
import Mission from "../components/landingPage/mission";
import Strengths from "../components/landingPage/strengths";

import "./../sass/style.scss";
import AboutUs from "../components/landingPage/aboutUs";

const Home: NextPage = () => (
    <div>
        <Head>
            <title>Home</title>
            <link rel="icon" href="/favicon.ico"/>
        </Head>

        <Banner/>

        <Cards/>

        <Mission/>

        <Strengths/>

        <AboutUs/>

    </div>
);

export default Home;
