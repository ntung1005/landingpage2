import React from 'react';
import Layout from "../components/layout";
import css from "../sass/pages/example.scss";
import Button from '@material-ui/core/Button';

const Example = () => (
  <div>
    <Layout>
      <p className={css.example}>Example</p>
      <Button color="primary">
        Example button
      </Button>
    </Layout>
  </div>
);

export default Example;
