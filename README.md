### Mô tả
Chứa các develop components . Mỗi branch là một component.
#### Cách làm việc
* Bước 1: Checkout sang branch được giao. Ví dụ branch `component/header`
* Bước 2: Viết code trên branch đó, push lên khi hoàn thành và báo với Leader để review.
#### Code flow
* Sử dụng editorconfig để chuẩn hoá code.
* Thư viện được phép sử dụng: [React Material UI](https://material-ui.com/)
* Sử dụng SASS để style component. Tham khảo `pages/example`. Các file styles nằm trong folder `sass`
